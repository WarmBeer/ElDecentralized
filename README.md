# ElDecentralized
A decentralized master server for ElDewrito

BASED ON https://github.com/ElDewrito/ElDewrito-MasterServer
BY: medsouz

DECENTRALIZED MASTER SERVER CONCEPT BY WARM_BEER (15-06-2019)
VERSION 3.2 (17-06-2019)

Clients would have to sync with one seeder on start to get an entire seeder_list to choose the best seeder from on next start.
/list and /announcing works just like the original master server.

Use Node JS to run.

UPPDATE:
V 3.2 Seeders are now verified before being added and offline seeders are automatically removed (except for the initial seeder(s))
